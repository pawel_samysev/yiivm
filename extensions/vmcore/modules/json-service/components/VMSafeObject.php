<?php
/**
 * @class VMSafeObject
 * Description of VMSafeObject class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 *
 * @property stdClass $node
 */
class VMSafeObject extends CComponent {
	/**
	 * @var stdClass
	 */
	private $object;

	/**
	 * @param array|stdClass $object
	 */
	public function __construct($object = null) {
		if (is_array($object)) {
			$object = VMObjectUtils::fromArray($object);
		}

		$this->object = $object;
	}

	public function __get($name) {
		if (parent::__isset($name)) {
			return parent::__get($name);
		}

		$isset = ($this->object && isset($this->object->{$name}));
		if ($isset) {
			return (!is_object($this->object->{$name})) ? $this->object->{$name} : new VMSafeObject($this->object->{$name});
		}

		return new VMSafeObject();
	}

	public function __isset($name) {
		if (!parent::__isset($name)) {
			return ($this->object) && isset($this->object->{$name});
		}

		return true;
	}

	public function itemAt($attributeName, $defaultValue = null) {
		$value = $this->{$attributeName};

		if ($value instanceof VMSafeObject) {
			if ($defaultValue === null) {
				$defaultValue = new VMSafeObject();
			}

			return ($value->object) ? $value->object : $defaultValue;
		}

		return $value;
	}

	public function getNode() {
		return $this->object;
	}

	public function __toString() {
		return ($this->object) ? (string) $this->object : '';
	}
} 