<?php

class VMEmailSender extends CComponent
{
	public static function send($recipients, $subject, $message = null, $sender = null)
	{
		if (!$sender) {
			$sender = Yii::app()->params->itemAt('adminEmail');
		}

		$headers = 'From: ' . $sender . PHP_EOL .
			'Reply-To: ' . $sender . PHP_EOL .
			'X-Mailer: PHP/' . phpversion() . PHP_EOL .
			'Content-Type: text/html; charset=utf-8' . PHP_EOL .
			'Content-Transfer-Encoding: 7bit';


		if (is_array($recipients)) {
			foreach ($recipients as $recipient) {
				mail($recipient, $subject, $message, $headers);
			}
		} elseif (is_string($recipients)) {
			mail($recipients, $subject, $message, $headers);
		}
	}
}