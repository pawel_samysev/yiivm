<?php

return array(
    'Access denied' => 'Доступ запрещен',
    'There is no request node' => 'Неверно сформированный запрос',
    'Missed input parameter -> {key}' => 'Пропущен входной параметр -> {key}',
    'Expected array in input parameter -> {key}' => 'Ожидаемый массив входных параметров -> {key}',
    'Invalid token or authorization issue' => 'Неверный токен или авторизация',
    'Method' => 'Метод',
    'Request' => 'Запрос',
    'Response' => 'Ответ',
);