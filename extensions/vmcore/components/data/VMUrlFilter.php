<?php
/**
 * @class VMUrlFilter
 * Description of VMUrlFilter class
 *
 * @author Egor Niloaev <egor@voodoo-mobile.com>
 */
class VMUrlFilter extends CComponent
{
	/**
	 * @param string $url
	 *
	 * @return string
	 */
	public function fullAddress($url = '')
	{
		if ($url) {
			$scheme = parse_url($url, PHP_URL_SCHEME);

			if (empty($scheme)) {
				$url = sprintf('%s://%s','http', $url);
			}
		}

		return $url;
	}
}