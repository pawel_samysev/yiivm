<?php

class VMDebugModeModifier extends VMConfigModifier
{
	public $enabled = false;

	public function run()
	{
		if ($this->enabled) {
			defined('YII_DEBUG') or define('YII_DEBUG', true);
		}
	}
}