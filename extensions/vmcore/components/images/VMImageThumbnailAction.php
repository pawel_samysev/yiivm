<?php
Yii::import('yiivm.extensions.image.*');
class VMImageThumbnailAction extends CAction
{
    public $thumbnailWidth = 200;
    public $thumbnailHeight = 200;
    private $image;
    private $size;

    public function run($path = NULL, $width = NULL, $height = NULL)
    {
        if ($width) {
            $this->thumbnailWidth = $width;
            $this->thumbnailHeight = $width;
        }
        if ($height) {
            $this->thumbnailHeight = $height;
        }

        $this->image = new Image($path);

        $this->makeAlignImageSize();

        $this->image->resize($this->size->width, $this->size->height, Image::NONE);
        $this->image->crop($this->thumbnailWidth, $this->thumbnailHeight);
        $this->image->render();

        Yii::app()->end();

    }

    private function makeAlignImageSize()
    {
        $size = (object)array(
            'realHeight' => $this->image->height,
            'realWidth' => $this->image->width,
            'customWidth' => $this->thumbnailWidth,
            'customHeight' => $this->thumbnailHeight
        );

        $size->alignHeight = $size->realHeight * ($size->customWidth / $size->realWidth);
        $size->alignWidth = $size->realWidth * ($size->customHeight / $size->realHeight);
        $size->differenceHeight = $size->alignHeight - $size->customHeight;
        $size->differenceWidth = $size->alignWidth - $size->customWidth;

        $isSmall = $size->realWidth < $size->customWidth && $size->realHeight < $size->customHeight;
        $isBig = $size->realWidth > $size->customWidth && $size->realHeight > $size->customHeight;

        if ($isSmall) {
            $this->size = $this->sizeSmallToBig($size);
        } else if ($isBig) {
            $this->size = $this->sizeBigToSmall($size);
        } else if ($size->differenceHeight > $size->differenceWidth) {
            $this->size = $this->alignWidth($size);
        } else {
            $this->size = $this->alignHeight($size);
        }
    }

    private function sizeSmallToBig($size)
    {
        if ($size->differenceHeight > $size->differenceWidth) {
            return $this->alignHeight($size);
        }
        return $this->alignWidth($size);
    }

    private function sizeBigToSmall($size)
    {
        if ($size->differenceWidth < $size->differenceHeight) {
            return $this->alignHeight($size);
        }
        return $this->alignWidth($size);
    }

    private function alignWidth($size)
    {
        return (object)array(
            'height' => $size->customHeight,
            'width' => $size->alignWidth
        );
    }

    private function alignHeight($size)
    {
        return (object)array(
            'height' => $size->alignHeight,
            'width' => $size->customWidth
        );
    }
}